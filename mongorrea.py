﻿# titulo           :mongorrea.py
# descripcion     : funciones para la base de datos MONGODB
# autor          : Ariel LK
# fecha            : 2019
# notas           :
# python_version  :3.0 +
# =======================================================================

import pymongo

dbcliente = pymongo.MongoClient("mongodb://localhost:27017/")


def dbexiste():
    dbs = dbcliente.list_database_names()
    if "dbelecciones2019" in dbs:
        return True
    else:
        return False


def creardb():
    eleccionesdb = dbcliente["dbelecciones2019"]
    coleccioncandidatos = eleccionesdb["candidatos"]
    candidatosdata = [
        {"_id": 1, "id": "ma", "nombre": "Marco Ameglio", "partido": "Libre Postulación", "votos": 0},
        {"_id": 2, "id": "jb", "nombre": "José Isabel Blandon", "partido": "Partido Panameñista", "votos": 0},
        {"_id": 3, "id": "lc", "nombre": "Laurentino Cortizo", "partido": "PRD", "votos": 0},
        {"_id": 4, "id": "ag", "nombre": "Ana Matilde Gomez", "partido": "Libre Postulación", "votos": 0},
        {"_id": 5, "id": "rl", "nombre": "Ricardo Lombana", "partido": "Libre Postulación", "votos": 0},
        {"_id": 6, "id": "sm", "nombre": "Saúl Mendez", "partido": "Frente Amplio por la Democracia", "votos": 0},
        {"_id": 7, "id": "rr", "nombre": "Rómulo Roux", "partido": "Cambio Democratico", "votos": 0}
    ]
    coleccioncandidatos.insert_many(candidatosdata)
    print("Datos insertados en la base de datos")


# recorremos la coleccion para ver si totem ( Valor ) existe
def dbtotemexiste(coleccion, totem):
    eleccionesdb = dbcliente["dbelecciones2019"]
    collector = eleccionesdb[coleccion]
    totems = collector.find({"id": totem}).count()
    if totems == 1:
        return True
    else:
        return False


def actualizarvoto(candidato):
    eleccionesdb = dbcliente["dbelecciones2019"]
    coleccioncandidatos = eleccionesdb["candidatos"]
    coleccioncandidatos.update_one({'nombre': candidato}, {'$inc': {'votos': 1}}, upsert=True)


def registrarvoto(cedula, candidato, partido):
    eleccionesdb = dbcliente["dbelecciones2019"]
    coleccioncedulas = eleccionesdb["votos"]
    pos = coleccioncedulas.count()
    pos = pos + 1
    voto = {"_id": pos, "id": cedula, "candidato": candidato, "partido": partido}
    coleccioncedulas.insert_one(voto)
    print("Cedula: " + cedula + " ha votado por el candidato: " + candidato + " del partido: " + partido)


def listardb(coleccion):
    eleccionesdb = dbcliente["dbelecciones2019"]
    collector = eleccionesdb[coleccion]
    return collector.find()


def listardbparam(coleccion, llave, valor):
    eleccionesdb = dbcliente["dbelecciones2019"]
    collector = eleccionesdb[coleccion]
    query = {llave: valor}
    return collector.find(query)


def listarmaxmindb(coleccion, criterio):
    eleccionesdb = dbcliente["dbelecciones2019"]
    collector = eleccionesdb[coleccion]
    return collector.find().sort(criterio, pymongo.DESCENDING)


def votostotales(coleccion, llave):
    total = 0
    eleccionesdb = dbcliente["dbelecciones2019"]
    collector = eleccionesdb[coleccion]
    doc = collector.find()
    try:
        for i in doc:
            total = total + i[llave]
    except:
        total = 0
    return total


def buscardb(coleccion, llave, valor, filtro):
    encontrado = None
    eleccionesdb = dbcliente["dbelecciones2019"]
    collector = eleccionesdb[coleccion]
    query = {llave: valor}
    doc = collector.find(query)
    for x in doc:
        encontrado = x[filtro]
    return encontrado
