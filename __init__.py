#!/usr/bin/env python
# -*- coding: utf-8 -*-
# titulo           :__init__.py
# descripcion     : Proyecto final: POLL 507
# fecha            : 2019
# version         :1.0
# Materia          :  Programacion 4 UIP , Profesor: Abdel Martinez
# Lib usadas  :  FLASK , MONGODB, BOOTSTRAP , CanvasJS
# Por:      Ariel Ortega
# gitlab: https://gitlab.com/saviorcode/poll507
# =======================================================================

from flask import Flask, render_template, request, redirect, Session
from mongorrea import dbexiste, creardb, listardb, buscardb, registrarvoto, actualizarvoto, dbtotemexiste, votostotales, \
    listarmaxmindb, listardbparam

from cedula import validate as validar


app = Flask(__name__)
app.secret_key = 'pirataquerobapirataesmaspirataqueprimerpirataquelerobóalpirataoriginal'
votante = Session()
votante['cedula'] = None

cedulainvalida = """<div class="alert alert-dismissible alert-danger">
<strong>Error!</strong> <a href="#" class="alert-link">Cedula o dato incorrecto</a> Intente nuevamente.
</div>"""

votoprohibido = """<div class="alert alert-dismissible alert-warning">
                    <h4 class="alert-heading">Prohibido!</h4>
                    <p class="mb-0">No puedes votar nuevamente, solo es valido la votacion unica por persona.</p>
                </div> """


@app.route('/')
def root():
    if dbexiste():
        return render_template('intro.html')
    else:
        creardb()
        return render_template('intro.html')


@app.route('/cedularegistro')
def cedreg():
    votante.pop('cedula', None)
    return render_template('registro.html')


@app.route('/listacandidatos', methods=['GET', 'POST'])
def enlistarcandidatos():

    if request.method == 'GET':
        return redirect('/cedularegistro')
    elif request.method == 'POST':
        ced = request.form['cedula']
        # Validamos la cedula ( si es valida | si esta completa )
        if validar(ced)["is_valid"] and validar(ced)["is_complete"]:
            if dbtotemexiste("votos", ced):
                return render_template('registro.html', incorrectid=votoprohibido)
            else:
                votante['cedula'] = ced
                return render_template('candidatos.html', encdata=listardb("candidatos"))
        else:
            return render_template('registro.html', incorrectid=cedulainvalida)


@app.route('/votar/<candidatoid>', methods=['GET', 'POST'])
def votarpor(candidatoid):
    if request.method == 'GET':
        return redirect('/cedularegistro')
    elif request.method == 'POST':
        # verificamos nuevamente si la cedula existe en la base de datos. ( FIX UPDATE : Ariel )
        if dbtotemexiste("votos", votante['cedula']):
            return render_template('registro.html', incorrectid=votoprohibido)
        else:
            registrarvoto(votante['cedula'], buscardb("candidatos", "id", candidatoid, "nombre"), buscardb("candidatos", "id", candidatoid, "partido"))
            actualizarvoto(buscardb("candidatos", "id", candidatoid, "nombre"))
            return render_template('final.html', candidato=buscardb("candidatos", "id", candidatoid, "nombre"), partido=buscardb("candidatos", "id", candidatoid, "partido"))


@app.route('/resultados')
def show_results():

    return render_template('resultados.html', titulo="Elecciones Panama 2019", resultado=listardb("candidatos"), resultado2=listarmaxmindb("candidatos", "votos"), total=votostotales("candidatos", "votos"))


@app.route('/easteregg')
def eastershow():
    return render_template('bd.html', ma=listardbparam("votos", "candidato", "Marco Ameglio"), jb=listardbparam("votos", "candidato", "José Isabel Blandon"),
                           lc=listardbparam("votos","candidato","Laurentino Cortizo"), ag=listardbparam("votos","candidato","Ana Matilde Gomez"),
                           rl=listardbparam("votos", "candidato","Ricardo Lombana"), sm=listardbparam("votos", "candidato", "Saúl Mendez"),
                           rr=listardbparam("votos", "candidato", "Rómulo Roux"))


if __name__ == "__main__":
    app.run(port=80)
